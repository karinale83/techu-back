var express = require('express');
var data = require('./data/user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
const global = require('./global');
var requestJSON = require('request-json');
var  baseMLabURL = 'https://api.mlab.com/api/1/databases/techu62db/collections/';
//const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
require('dotenv').config();
const apikeyMLab = "apiKey=" + process.env.MLAB_API_KEY;

app.use(express.json());

var users = require('./controllers/users.js');
var auth = require('./controllers/auth.js');

// app.get(global.URL_BASE + "users/:id", users.getUserById);
// app.post(global.URL_BASE + "users", users.postUsers);
// app.put(global.URL_BASE + "users", users.putUsers);
// app.delete(global.URL_BASE + "users/:id", users.deleteUsers);
//
// app.post(global.URL_BASE + "login", auth.login);
// app.post(global.URL_BASE + "logout/:id", auth.logout);

//GET users consumiendo API REST de mLab
app.get(global.URL_BASE + 'users',
 function(req, res) {
   console.log("GET /techu-peru/v3/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('users?' + queryString + apikeyMLab,
     function(error, respuestaMLab, body) {
       var response = {};
       if(error) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET users consumiendo API REST de mLab
app.get(global.URL_BASE + 'users/:id',
 function(req, res) {
   console.log("GET /techu-peru/v3/users/:id");
   console.log(req.param.id);
   var id = req.param.id;
   var queryString = 'q={"ID":' + id + '}&';
   var queyStrField = 'f={"_id":0}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('users?' + queryString + queyStrField + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log("Respuesta mLab correcta.");
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

//GET accounts de un user
app.get(global.URL_BASE + 'users/:id/accounts',
 function(req, res) {
   console.log("GET /techu-peru/v3/users/:id/accounts");
   console.log(req.param.id);
   var id = req.param.id;
   var queryString = 'q={"id_user":' + id + '}&';
   var queyStrField = 'f={"_id":0}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('accounts?' + queryString + queyStrField + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log("Respuesta mLab correcta.");
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// Implementar el POST
//??????????????
app.post(global.URL_BASE + 'users',
   function(req,res){
       console.log("POST /techu-peru/v3/users");
       var httpClient = requestJSON.createClient(baseMLabURL);
       console.log(req.body);
       httpClient.get('users?' + apikeyMLab,
          function(error, respuestaMLab, body){
              newID = body.length + 1;
              console.log("newID:" + newID);
              var newUser ={
                  "id" : newID,
                  "first_name" : req.body.first_name,
                  "last_name" : req.body.last_name,
                  "email" : req.body.email,
                  "password" : req.body.password
              };
              //se puede omitir el pasarle la baseMLabURL
              httpClient.post(baseMLabURL + "users?" + apikeyMLab, newUser,
                  function(error, respuestaMLab, bodyPost){
                      console.log(bodyPost);
                      console.log(baseMLabURL + "users?" + apikeyMLab);
                      var response = {};
                      if(error) {
                          response = {"msg" : "Error obteniendo usuario."}
                          res.status(500);
                      } else {
                         response = bodyPost;
                        }
                      res.send(response);
                    });
   });
 });


//PUT users con parámetro 'id'
app.put(global.URLbase + 'users/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var httpClient = requestJSON.createClient(baseMLabURL);
 httpClient.get('users?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    httpClient.put(baseMLabURL +'users?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});

// Petición PUT con id de mLab (_id.$oid)
 app.put(global.URLbase + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('users?' + queryString + apikeyMLab,
       function(error, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('users/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(error, respuestaMLab, body){
             var response = {};
             if(error) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });

 // LOGIN
 // let login = '{"$set":{"logged":true}}';
 //            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(login),
 //            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
 //              function(errPut, resPut, bodyPut) {
 // DELETE users
 // //DELETE user with id
 // app.delete(URLbase + "users/:id",
 //  function(req, res){
 //    var id = req.params.id;
 //    var queryStringID = 'q={"id":' + id + '}&';
 //    console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
 //    var httpClient = requestJSON.createClient(baseMLabURL);
 //    httpClient.get('user?' +  queryStringID + apikeyMLab,
 //      function(error, respuestaMLab, body){
 //        var respuesta = body[0];
 //        httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
 //          function(error, respuestaMLab,body){
 //            res.send(body);
 //        });
 //      });
 //  });


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
