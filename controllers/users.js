var express = require('express');
var user_file = require('../data/user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Petición GET users
// app.get(URL_BASE + 'users',
//   function (req, res) {
//   //res.send({"msg":"Operacion GET exitosa"});
//   //res.send(user_file);
//   res.status(201).send(user_file);
// });

//Peticiòn GET users con ID
function getUserById(req,res){
   let pos = req.params.id -1;
   console.log('GET con id =' + req.params.id );
   let tam = user_file.length;
   console.log(tam);
   let respuesta = (user_file[pos] == undefined) ? {"msg":"Usuario mo encontrado"} : user_file[pos];
   res.send(respuesta);
 };

//Petición GET users con Query String
// function getUsers(req,res){
//       console.log(req.query.id);
//       console.log(req.query.name);
//   });

// POST users

function postUsers(req,res){
   console.log('POST de users');
  console.log('Nuevo Usuario:' + req.body);
  //  console.log('Nuevo Usuario:' + req.body.first_name);
  //  console.log('Nuevo Usuario:' + req.body.last_name);
  let newID = user_file.length +1;
  let newUser = {
     "id": newID,
     "first_name": req.body.first_name,
     "last_name": req.body.last_name,
     "email": req.body.email,
     "password": req.body.password
  };
  user_file.push(newUser);
  console.log("Nuevo usuario: " + newUser);
  res.send(newUser);
  //res.send({"msg":"POST exitoso"});

 };

// PUT users

function putUsers(req,res){
   console.log('POST de users');
  //  console.log('Nuevo Usuario:' + req.body);
  //  console.log('Nuevo Usuario:' + req.body.first_name);
  //  console.log('Nuevo Usuario:' + req.body.last_name);
  let pos = req.body.id -1;
  let updatedUser = {
     "id": req.body.id,
     "first_name": req.body.first_name,
     "last_name": req.body.last_name,
     "email": req.body.email,
     "password": req.body.password
  };
  //user_file.fill(updatedUser,req.body.id-1,1);
  user_file[pos] = updatedUser;
  console.log("Usuario actualizado: " + updatedUser);
  res.send(updatedUser);
  //res.send({"msg":"POST exitoso"});

 };

//DELETE users
function deleteUsers(req, res){
 console.log('Delete the user,');
 let pos = req.params.id;
 let resp = (user_file[pos] == undefined) ? {"msg":"Usuario no existe"} : user_file.splice(pos-1,1);
 res.send({"msg":"Usuario eliminado"});
};

module.exports.getUserById = getUserById;
module.exports.postUsers = postUsers;
module.exports.putUsers = putUsers;
module.exports.deleteUsers = deleteUsers;
