var express = require('express');
var user_file = require('../data/user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;

app.use(bodyParser.json());

// login
function login(req,res){
  console.log('Login ' + req.body.email);
  let tam = user_file.length;
  let i = 0;
  let encontrado = false;
  while ((i < user_file.length) && !encontrado) {
    if (user_file[i].email == req.body.email && user_file[i].password == req.body.password)
    {
      encontrado = true;
      user_file[i].logged = true;
      writeUserDataToFile(user_file);
    } i++;
}
if (encontrado)
  res.send({"msg":"Usuario logueado correctamente", "id":i, "email": user_file[i].email})
else {
   res.send({"msg":"Usuario no encontrado"})

}
};

// Logout
function logout(req,res){
     console.log('User logout');
     let pos = req.params.id-1;
     if (user_file[pos].ID != undefined)
     {
       //user_file[req.params.id-1].logged = false;
       user_file.splice(pos,6);
       writeUserDataToFile(user_file);
       res.send({"msg":"Logout correcto " + user_file[req.params.id-1].email});

     }
     else{
        res.send({"msg":"Usuario incorrecto"})
      }
};

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 };

 module.exports = {
       login,
       logout
   }
